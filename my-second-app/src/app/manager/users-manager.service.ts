import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserService } from '../servicios/user.service';

@Injectable({
  providedIn: 'root'
})
export class UsersManagerService {

  private _loading:BehaviorSubject<any> = new BehaviorSubject(false);
  private _user:BehaviorSubject<any> = new BehaviorSubject(false);
  
  constructor(private userService:UserService) { }

  getLoading(){
    return this._loading.asObservable();
  }

  getListUser(){
    return this._user.asObservable();
  }

  getUsers(){
    this._loading.next(true);
    this.userService.getCliente().subscribe((resp) => {
      this._loading.next(false);
      this._user.next(resp);
    },(err)=>{
      this._loading.next(false);
    });
  }
}
