import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ArticulosService } from '../servicios/articulos.service';

@Injectable({
  providedIn: 'root'
})
export class ArticuloManagerService {

  private loading_: BehaviorSubject<any> = new BehaviorSubject(false);
  private articulos_: BehaviorSubject<any> = new BehaviorSubject(false);

  constructor(private articuloService:ArticulosService) { }

  getLoading(){
    return this.loading_.asObservable();
  }

  getListArticulo(){
    return this.articulos_.asObservable();
  }

  getAllArticulo(){
    this.loading_.next(true);
    this.articuloService.getArticulos().subscribe( resp => {
      this.loading_.next(false);
      this.articulos_.next(resp);
    },(err)=> {
      this.loading_.next(false);
    });
  }

}
