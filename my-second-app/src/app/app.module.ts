import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Router, RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { ArticuloComponent } from './componentes/articulo/articulo.component';
import { HttpClientModule } from '@angular/common/http';
import { UserComponent } from './componentes/user/user.component';
import { DetalleArticuloComponent } from './componentes/detalle-articulo/detalle-articulo.component';

const routes:Routes =[
  {path:'articulos',component:ArticuloComponent},
  {path:'users',component:UserComponent},
  {path:'detalle-articulo/:id/:name/:category/:price/:image_link/:price_sign/:description',component:DetalleArticuloComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    ArticuloComponent,
    UserComponent,
    DetalleArticuloComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule
  ],
  exports:[
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
