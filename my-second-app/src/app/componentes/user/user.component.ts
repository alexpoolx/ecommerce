import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/servicios/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.sass']
})
export class UserComponent implements OnInit {

  users: any = [];
  my_user: any = [];
  constructor(private Service:UserService) {
  }

  ngOnInit(): void {
    this.Service.getCliente().subscribe(data => {
      this.users = data;
      console.log(this.users);
    });

  }

  getAnyUser(){
    console.log();
  }
}
