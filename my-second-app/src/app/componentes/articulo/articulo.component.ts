import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ArticuloManagerService } from 'src/app/manager/articulo-manager.service';
import { Articulos } from 'src/app/modelos/Articulos';
import { ArticulosService } from 'src/app/servicios/articulos.service';

@Component({
  selector: 'app-articulo',
  templateUrl: './articulo.component.html',
  styleUrls: ['./articulo.component.sass']
})
export class ArticuloComponent implements OnInit {

  articulos:Articulos[]= [];
  subscripcion = new Subscription();
  evnt: boolean = false;
  constructor(private serviceManager:ArticuloManagerService) { }

  ngOnInit(): void {
    this.serviceManager.getAllArticulo();
    this.serviceManager.getListArticulo().subscribe((resp) => {
      this.articulos = resp;
      //console.log(this.articulos);
    });
  }

  verArticulo(item:Articulos){
    console.log(item);
  }
  

}
