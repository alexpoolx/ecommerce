import { Component, Input, OnInit } from '@angular/core';
import { Articulos } from 'src/app/modelos/Articulos';
import { ArticuloComponent } from '../articulo/articulo.component';
import { ActivatedRoute, Params } from '@angular/router';
@Component({
  selector: 'app-detalle-articulo',
  templateUrl: './detalle-articulo.component.html',
  styleUrls: ['./detalle-articulo.component.sass']
})
export class DetalleArticuloComponent implements OnInit {
  //@Input() detalleArt:Articulos[] = [] ;
  detalleArticulo:any []= [];
  price_sign:string = "";
  constructor(private rutaArticulo:ActivatedRoute) { }
  ngOnInit(): void {
      this.detalleArticulo = [{
        id : this.rutaArticulo.snapshot.paramMap.get('id'),
        name : this.rutaArticulo.snapshot.paramMap.get('name'),
        category : this.rutaArticulo.snapshot.paramMap.get('category'),
        price : this.rutaArticulo.snapshot.paramMap.get('price'),
        image_link : this.rutaArticulo.snapshot.paramMap.get('image_link'),
        price_sign : this.rutaArticulo.snapshot.paramMap.get('price_sign'),
        description: this.rutaArticulo.snapshot.paramMap.get('description')
      }];

     /*this.rutaArticulo.params.subscribe(
      (params: Params) => {
        this.detalleArticulo.name = params.name;
      }
    );*/
    
    !this.price_sign ? this.price_sign = "$" : this.price_sign;
  }

}
